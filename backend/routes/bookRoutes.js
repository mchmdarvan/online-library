const express = require("express");
const {
	index,
	borrowBook,
	getBorrowedUsers,
	returnBook,
} = require("../controllers/bookControllers");
const authenticateToken = require("../middleware/authenticateToken");
const checkAdmin = require("../middleware/checkAdmin");

const router = express.Router();

router.post("/borrow", authenticateToken, borrowBook);
router.get(
	"/admin/borrowed-users",
	authenticateToken,
	checkAdmin,
	getBorrowedUsers
);
router.post("/return", authenticateToken, returnBook);
router.get("/book", index);

module.exports = router;
