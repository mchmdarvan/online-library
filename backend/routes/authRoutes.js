const express = require("express");
const {
	register,
	adminRegister,
	login,
} = require("../controllers/authControllers");

const router = express.Router();

router.post("/register", register);
router.post("/admin-register", adminRegister);
router.post("/login", login);

module.exports = router;
