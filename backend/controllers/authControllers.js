require("dotenv").config();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { query } = require("../utils/database");

async function register(req, res) {
	try {
		const { email, password } = req.body;

		// Validasi email
		const validEmailDomains = ["gmail.com", "hotmail.com"]; // Domain yang diizinkan
		const emailDomain = email.split("@")[1];
		if (!validEmailDomains.includes(emailDomain)) {
			return res.status(400).json({ error: "Invalid email domain" });
		}

		// Validasi password 8 karakter alphanumerik dan 1 huruf kapital
		if (!/^(?=.*[A-Z])(?=.*[0-9a-z]).{8,}$/.test(password)) {
			return res.status(400).json({
				error: "Invalid password. Password must be at least 8 characters long and contain at least 1 uppercase letter.",
			});
		}

		const existingUser = await query(
			"SELECT * FROM users WHERE email = ?",
			[email]
		);

		if (existingUser.length > 0) {
			return res
				.status(400)
				.json({ error: "Email is already registered" });
		}

		const saltRounds = 10;
		const hashedPassword = await bcrypt.hash(password, saltRounds);

		await query("INSERT INTO users (email, password) VALUES (?, ?)", [
			email,
			hashedPassword,
		]);

		res.status(201).json({ message: "User registered successfully" });
	} catch (error) {
		console.error(error);
		res.status(500).json({ error: "Internal server error" });
	}
}

async function adminRegister(req, res) {
	try {
		const { email, password } = req.body;

		// Validasi email
		const validEmailDomains = ["gmail.com", "hotmail.com"]; // Domain yang diizinkan
		const emailDomain = email.split("@")[1];
		if (!validEmailDomains.includes(emailDomain)) {
			return res.status(400).json({ error: "Invalid email domain" });
		}

		// Validasi password 8 karakter alphanumerik dan 1 huruf kapital
		if (!/^(?=.*[A-Z])(?=.*[0-9a-z]).{8,}$/.test(password)) {
			return res.status(400).json({
				error: "Invalid password. Password must be at least 8 characters long and contain at least 1 uppercase letter.",
			});
		}

		const existingUser = await query(
			"SELECT * FROM users WHERE email = ?",
			[email]
		);

		if (existingUser.length > 0) {
			return res
				.status(400)
				.json({ error: "Email is already registered" });
		}

		const saltRounds = 10;
		const hashedPassword = await bcrypt.hash(password, saltRounds);

		await query(
			"INSERT INTO users (email, password, role) VALUES (?, ?, ?)",
			[email, hashedPassword, "admin"]
		);

		res.status(201).json({ message: "User registered successfully" });
	} catch (error) {
		console.error(error);
		res.status(500).json({ error: "Internal server error" });
	}
}

async function login(req, res) {
	try {
		const { email, password } = req.body;

		const user = await query("SELECT * FROM users WHERE email = ?", [
			email,
		]);

		if (user.length === 0) {
			return res.status(401).json({ error: "Invalid credentials" });
		}

		const isPasswordValid = await bcrypt.compare(
			password,
			user[0].password
		);

		if (!isPasswordValid) {
			return res.status(401).json({ error: "Invalid credentials" });
		}

		const token = jwt.sign(
			{
				userId: user[0].user_id,
				email: user[0].email,
				role: user[0].role,
			},
			process.env.ACCESS_TOKEN_SECRET,
			{ expiresIn: "7d" }
		);

		res.json({ message: "Login successful", token });
	} catch (error) {
		console.error(error);
		res.status(500).json({ error: "Internal server error" });
	}
}

module.exports = {
	register,
	login,
	adminRegister,
};
