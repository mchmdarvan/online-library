require("dotenv").config();
const jwt = require("jsonwebtoken"); // Import the 'jsonwebtoken' library

const { query } = require("../utils/database");

async function borrowBook(req, res) {
	try {
		const { userId, bookId } = req.body;

		// Pastikan token JWT disertakan dalam header permintaan
		const token = req.header("Authorization");
		const newToken = token && token.split(" ")[1];

		if (!token) {
			return res
				.status(401)
				.json({ error: "Authorization token is missing" });
		}

		jwt.verify(
			newToken,
			process.env.ACCESS_TOKEN_SECRET,
			async (err, decoded) => {
				// Added async keyword here
				if (err) {
					return res.status(401).json({ error: "Invalid token" });
				}

				// Token valid, cek apakah user ID di dalam token cocok dengan yang diminta untuk meminjam buku
				if (decoded.userId !== userId) {
					return res
						.status(403)
						.json({ error: "Forbidden: User ID mismatch" });
				}

				// Check if the user has already borrowed a book
				const existingBorrowing = await query(
					"SELECT * FROM borrowings WHERE user_id = ? AND book_id = ? AND return_date != NULL",
					[userId, bookId]
				);

				if (existingBorrowing.length > 0) {
					return res
						.status(400)
						.json({ error: "User has already borrowed this book" });
				}

				// Check if the book is available
				const book = await query(
					"SELECT * FROM books WHERE book_id = ? AND available = true",
					[bookId]
				);

				if (book.length === 0) {
					return res
						.status(400)
						.json({ error: "Book is not available for borrowing" });
				}

				// Record the borrowing in the borrowings table
				await query(
					"INSERT INTO borrowings (user_id, book_id, borrow_date) VALUES (?, ?, CURDATE())",
					[userId, bookId]
				);

				// Update the availability of the book
				await query(
					"UPDATE books SET available = false WHERE book_id = ?",
					[bookId]
				);

				res.json({ message: "Book borrowed successfully" });
			}
		);
	} catch (error) {
		console.error(error);
		res.status(500).json({ error: "Internal server error" });
	}
}

async function getBorrowedUsers(req, res) {
	try {
		// Query to get users who have borrowed books
		const borrowedUsers = await query(`
            SELECT users.user_id as user_id, users.email, books.book_id as book_id, books.title, borrowings.borrow_date, borrowings.return_date
            FROM users
            JOIN borrowings ON users.user_id = borrowings.user_id
            JOIN books ON borrowings.book_id = books.book_id
        `);

		// You can further customize the query to fetch additional information if needed

		res.json({ borrowedUsers });
	} catch (error) {
		console.error(error);
		res.status(500).json({ error: "Internal server error" });
	}
}

async function returnBook(req, res) {
	try {
		const { userId, bookId } = req.body;

		const token = req.header("Authorization");
		const newToken = token && token.split(" ")[1];

		if (!token) {
			return res
				.status(401)
				.json({ error: "Authorization token is missing" });
		}
		jwt.verify(
			newToken,
			process.env.ACCESS_TOKEN_SECRET,
			async (err, decoded) => {
				// Added async keyword here
				if (err) {
					return res.status(401).json({ error: "Invalid token" });
				}

				// Token valid, cek apakah user ID di dalam token cocok dengan yang diminta untuk meminjam buku
				if (decoded.userId !== userId) {
					return res
						.status(403)
						.json({ error: "Forbidden: User ID mismatch" });
				}

				// Check if the book is currently borrowed by the user
				const isBorrowed = await query(
					"SELECT * FROM borrowings WHERE user_id = ? AND book_id = ? AND return_date IS NULL",
					[userId, bookId]
				);

				if (!isBorrowed.length) {
					return res.status(400).json({
						error: "Book not currently borrowed by the user",
					});
				}

				// Update the return_date for the borrowed book
				await query(
					"UPDATE borrowings SET return_date = CURRENT_DATE WHERE user_id = ? AND book_id = ?",
					[userId, bookId]
				);

				await query(
					"UPDATE books SET available = 1 WHERE book_id = ?",
					[bookId]
				);

				res.json({ message: "Book successfully returned" });
			}
		);
	} catch (error) {
		console.error(error);
		res.status(500).json({ error: "Internal server error" });
	}
}

async function index(req, res) {
	try {
		const listBook = await query("SELECT * FROM books");

		res.json(listBook);
	} catch (error) {
		console.error(error);
		res.status(500).json({ error: "Internal server error" });
	}
}

module.exports = {
	borrowBook,
	getBorrowedUsers,
	returnBook,
	index,
};
