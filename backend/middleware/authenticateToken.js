require("dotenv").config();
const jwt = require("jsonwebtoken");

function authenticateToken(req, res, next) {
	// Pastikan token JWT disertakan dalam header permintaan
	const token = req.header("Authorization");
	const newToken = token && token.split(" ")[1];

	if (!token) {
		return res
			.status(401)
			.json({ error: "Authorization token is missing" });
	}

	// Verifikasi token JWT
	jwt.verify(newToken, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
		if (err) {
			return res.status(401).json({ error: "Invalid token" });
		}

		req.user = decoded; // Menyimpan ID pengguna dalam objek permintaan untuk digunakan di rute berikutnya
		next(); // Melanjutkan ke rute berikutnya setelah autentikasi berhasil
	});
}

module.exports = authenticateToken;
