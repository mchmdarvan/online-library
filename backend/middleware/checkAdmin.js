function checkAdmin(req, res, next) {
	// Check if the user has the 'admin' role
	if (req.user && req.user.role === "admin") {
		next();
	} else {
		res.status(403).json({ error: "Forbidden: Admin access required" });
	}
}

module.exports = checkAdmin;
