const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const authRoutes = require("./routes/authRoutes");
const bookRoutes = require("./routes/bookRoutes");

const app = express();
const port = 3000;

app.use(cors());
app.use(bodyParser.json());

app.use("/auth", authRoutes); // Gunakan '/auth' sebagai awalan URL untuk semua rute di authRoutes
app.use("", bookRoutes);

app.listen(port, () => {
	console.log(`Server is running on port ${port}`);
});
