const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");

const app = express();

app.use(bodyParser.json());

// Connect to MySQL
const db = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "",
	database: "online-library",
});

db.connect((err) => {
	if (err) {
		console.error("Database connection failed:", err);
	} else {
		console.log("Connected to MySQL database");
	}
});
function query(sql, values) {
	return new Promise((resolve, reject) => {
		db.query(sql, values, (err, results) => {
			if (err) {
				reject(err);
			} else {
				resolve(results);
			}
		});
	});
}

module.exports = { query };
