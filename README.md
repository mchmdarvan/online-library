# online-library

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Export Database

Please export the database in folder dump, for better experience.

## Running Project

User doesn't have to run "Npm install" to run these project, you can just start by use "Npm Start".

## About these Project

These project was for user technical test from ifabula, for Backend Position. These project was support for JWT and Hashing for better Authentication.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

For open source projects, say how it is licensed.

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
